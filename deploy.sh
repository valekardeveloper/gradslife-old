echo "Deploy script started"


cd ~/project


wordpress='wordpress'
#x=$(docker ps -a | awk '/wordpress/ {print $0}') 

x=$(docker ps -a | awk -v word=$wordpress '$0~word' )

container_ID=$(echo $x | awk '{print $1}')

image_name=$(echo $x | awk '{print $2}')


echo $container_ID 
echo $image_name

#remove docker container
docker stop $container_ID
docker rm $container_ID

#remove docker image
docker rmi $image_name

# Start the network
network=$(docker network ls | awk '/nginxproxy_default/ {print $0}')
if [[ "$network" = "" ]]; then
    docker network create nginxproxy_default
	echo "Started network"
else 
	echo "Removing network...."
	docker network rm nginxproxy_default
	docker network create nginxproxy_default
	echo "Started network"
fi

#create the containers
sudo docker-compose up --force-recreate -d 

echo "Running the nginx Proxy..."
#docker pull developervalekar/nginx_deploy:v1
#docker run developervalekar/nginx_deploy:v1

echo "Making nginx_proxy directory"
rm -rf nginxproxy
#mkdir nginx_proxy
#cd nginx_proxy

echo "Pulling code from git"

git clone https://valekardeveloper@bitbucket.org/valekardeveloper/nginxproxy.git

cd nginxproxy
#sudo apt-get install build-essential libssl-dev libffi-dev python-dev

#echo "starting nginx proxy...."
docker-compose up --force-recreate -d

echo "Deploy script finished execution"


###########################################################
## Practiced code
#extracted=$(echo $(docker ps -a | awk '{print $11}'))  

#echo $extracted

#for name in $extracted; do
#	if [[ "$name" = "wordpress" ]]; then
#		get_wordpress=$name
#	fi
#done

#echo $get_wordpress

#x=$(docker ps -a | awk '${print $0}') 

#echo $x


